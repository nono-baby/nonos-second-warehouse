<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/user/list"/>
    <!-- </section> -->
    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>用户管理页面 >> 用户修改页面</span>
        </div>
        <div class="supplierAdd">
            <form id="userForm" name="userForm" method="post" action="/store724_war/sys/user/update">
                <input type="hidden" name="id" value="11"/>
                <div>
                    <label for="account">账号：</label>
                    <input type="text" readonly name="account" id="account" value="liuyang">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="realName">真实姓名：</label>
                    <input type="text" name="realName" id="realName" value="刘阳">
                    <font color="red"></font>
                </div>
                <div>
                    <label >性别：</label>
                    <select name="sex" id="sex">



                        <option value="1">女</option>
                        <option value="2" selected="selected">男</option>


                    </select>
                </div>
                <div>
                    <label for="birthday">出生日期：</label>
                    <input type="text" Class="Wdate" id="birthday" name="birthday" value="1978-03-12"
                           readonly="readonly" onclick="WdatePicker();">
                    <font color="red"></font>
                </div>

                <div>
                    <label for="phone">手机号码：</label>
                    <input type="text" name="phone" id="phone" value="13367890900">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="address">地址：</label>
                    <input type="text" name="address" id="address" value="北京市海淀区成府路207号">
                </div>
                <div>
                    <label >角色：</label>
                    <!-- 列出所有的角色分类 -->
                    <input type="hidden" value="2" id="rid" />
                    <select name="roleId" id="roleList"></select>

                    <font color="red"></font>
                </div>
                <div class="supplierAddBtn">
                    <input type="button" name="save" id="save" value="保存" />
                    <input type="button" id="back" name="back" value="返回"/>
                </div>
            </form>
        </div>
    </div>
</section>


<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/sysUser/update.js"></script>

