<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/user/list"/>
    <!-- </section> -->

    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>用户管理页面 >> 用户添加页面</span>
        </div>
        <div class="supplierAdd">
            <form id="userForm" name="userForm" enctype="multipart/form-data" method="post" action="/store724_war/sys/user/add">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div>
                    <label for="account">账号：</label>
                    <input type="text" name="account" id="account" value="">
                    <!-- 放置提示信息 -->
                    <font color="red"></font>
                </div>
                <div>
                    <label for="realName">真实姓名：</label>
                    <input type="text" name="realName" id="realName" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="password">密码：</label>
                    <input type="password" name="password" id="password" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="rpassword">确认密码：</label>
                    <input type="password" name="rpassword" id="rpassword" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label >性别：</label>
                    <select name="sex" id="sex">
                        <option value="1" selected="selected">女</option>
                        <option value="2">男</option>
                    </select>
                </div>
                <div>
                    <label for="birthday">出生日期：</label>
                    <input type="text" Class="Wdate" id="birthday" name="birthday"
                           readonly="readonly" onclick="WdatePicker();">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="phone">手机号码：</label>
                    <input type="text" name="phone" id="phone" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="address">地址：</label>
                    <input name="address" id="address"  value="">
                </div>
                <div>
                    <label >角色：</label>
                    <!-- 列出所有的角色分类 -->
                    <select name="roleId" id="roleList"></select>

                    <font color="red"></font>
                </div>
                <div>
                    <input type="hidden" id="errorinfo" value=""/>
                    <label for="idPic">证件照</label>
                    <input type="file" name="idPic" id="idPic"/>
                    <font color="red"></font>
                </div>
                <div>
                    <input type="hidden" id="errorinfo_wp" value=""/>
                    <label for="workPic">工作证照片：</label>
                    <input type="file" name="workPic" id="workPic"/>
                    <font color="red"></font>
                </div>
                <div class="supplierAddBtn">
                    <input type="button" name="add" id="add" value="保存" >
                    <input type="button" id="back" name="back" value="返回" >
                </div>
            </form>
        </div>
    </div>
</section>


<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/sysUser/add.js"></script>

