<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/role/list"/>
    <!-- </section> -->
    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>密码修改页面</span>
        </div>
        <div class="supplierAdd">
            <form id="userForm" name="userForm" method="post" action="/store724_war/user/savePassword">
                <input type="hidden" name="id" value="">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div class="info"></div>
                <div class="">
                    <label for="oldPassword">旧密码：</label>
                    <input type="password" name="oldPassword" id="oldpassword" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="newPassword">新密码：</label>
                    <input type="password" name="newPassword" id="newpassword" value="">
                    <font color="red"></font>
                </div>
                <div>
                    <label for="reNewPassword">确认新密码：</label>
                    <input type="password" name="reNewPassword" id="rnewpassword" value="">
                    <font color="red"></font>
                </div>
                <div class="supplierAddBtn">
                    <!--<a href="#">保存</a>-->
                    <input type="button" name="save" id="save" value="保存" class="input-button">
                </div>
            </form>
        </div>
    </div>
</section>


<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/sysUser/updatePassword.js"></script>
