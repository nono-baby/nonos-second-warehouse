<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/supplier/list"/>
    <!-- </section> -->

    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>供货商管理页面</span>
        </div>
        <div class="search">
            <form method="get" action="/store724_war/sys/supplier/list">
                <span>供货商编码：</span>
                <input name="querySupCode" type="text" value="">

                <span>供货商名称：</span>
                <input name="querySupName" type="text" value="">
                <input type="hidden" name="pageIndex" value="1"/>
                <input value="查 询" type="submit" id="searchbutton">
                <a href="/store724_war/sys/supplier/toAdd">添加</a>
            </form>
        </div>
        <!--供货商操作表格-->
        <table class="supplierTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">供货商编码</th>
                <th width="20%">供货商名称</th>
                <th width="10%">联系人</th>
                <th width="10%">联系电话</th>
                <th width="10%">传真</th>
                <th width="10%">创建时间</th>
                <th width="30%">操作</th>
            </tr>

            <tr>
                <td>
                    <span>HB_GYS001</span>
                </td>
                <td>
                    <span>石家庄帅益食品贸易有限公司</span>
                </td>
                <td>
                    <span>赵传军</span>
                </td>
                <td>
                    <span>13309094212</span>
                </td>
                <td>
                    <span>0311-67738876</span>
                </td>
                <td>
					<span>
					2016-04-13
					</span>
                </td>
                <td>
                    <span><a class="viewSupplier" href="javascript:;" supId=11 supName=石家庄帅益食品贸易有限公司><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifySupplier" href="javascript:;" supId=11 supName=石家庄帅益食品贸易有限公司><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteSupplier" href="javascript:;" supId=11 supName=石家庄帅益食品贸易有限公司><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>BJ_GYS003</span>
                </td>
                <td>
                    <span>中粮集团有限公司</span>
                </td>
                <td>
                    <span>王驰</span>
                </td>
                <td>
                    <span>13344441135</span>
                </td>
                <td>
                    <span>010-588134111</span>
                </td>
                <td>
					<span>
					2016-04-13
					</span>
                </td>
                <td>
                    <span><a class="viewSupplier" href="javascript:;" supId=7 supName=中粮集团有限公司><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifySupplier" href="javascript:;" supId=7 supName=中粮集团有限公司><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteSupplier" href="javascript:;" supId=7 supName=中粮集团有限公司><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>JS_GYS001</span>
                </td>
                <td>
                    <span>福建味美美调味品厂</span>
                </td>
                <td>
                    <span>徐国洋</span>
                </td>
                <td>
                    <span>13754444221</span>
                </td>
                <td>
                    <span>0523-21299098</span>
                </td>
                <td>
					<span>
					2015-11-22
					</span>
                </td>
                <td>
                    <span><a class="viewSupplier" href="javascript:;" supId=5 supName=福建味美美调味品厂><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifySupplier" href="javascript:;" supId=5 supName=福建味美美调味品厂><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteSupplier" href="javascript:;" supId=5 supName=福建味美美调味品厂><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>SD_GYS001</span>
                </td>
                <td>
                    <span>山东豪克华光联合发展有限公司</span>
                </td>
                <td>
                    <span>周康</span>
                </td>
                <td>
                    <span>13245468787</span>
                </td>
                <td>
                    <span>0531-53362445</span>
                </td>
                <td>
					<span>
					2015-01-28
					</span>
                </td>
                <td>
                    <span><a class="viewSupplier" href="javascript:;" supId=13 supName=山东豪克华光联合发展有限公司><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifySupplier" href="javascript:;" supId=13 supName=山东豪克华光联合发展有限公司><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteSupplier" href="javascript:;" supId=13 supName=山东豪克华光联合发展有限公司><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>GZ_GYS001</span>
                </td>
                <td>
                    <span>乐山市泰国大米有限公司</span>
                </td>
                <td>
                    <span>王圣丹</span>
                </td>
                <td>
                    <span>13402013312</span>
                </td>
                <td>
                    <span>0755-67776212</span>
                </td>
                <td>
					<span>
					2014-03-21
					</span>
                </td>
                <td>
                    <span><a class="viewSupplier" href="javascript:;" supId=3 supName=乐山市泰国大米有限公司><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifySupplier" href="javascript:;" supId=3 supName=乐山市泰国大米有限公司><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteSupplier" href="javascript:;" supId=3 supName=乐山市泰国大米有限公司><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

        </table>
        <input type="hidden" id="totalPageCount" value="2"/>






        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Insert title here</title>
            <script type="text/javascript">

            </script>
        </head>
        <body>
        <div class="page-bar">
            <ul class="page-num-ul clearfix">
                <li>共10条记录&nbsp;&nbsp; 1/2页</li>


                <a href="javascript:page_nav(document.forms[0],2);">下一页</a>
                <a href="javascript:page_nav(document.forms[0],2);">尾页</a>

                &nbsp;&nbsp;
            </ul>
            <span class="page-go-form"><label>跳转至</label>
	     <input type="text" name="inputPage" id="inputPage" class="page-key" />页
	     <button type="button" class="page-btn" onClick='jump_to(document.forms[0],document.getElementById("inputPage").value)'>GO</button>
		</span>
        </div>
        </body>
        <script type="text/javascript" src="/store724_war/statics/js/sysRole/rollpage.js"></script>
        </html>
    </div>
</section>

<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeProv">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain" >
            <p>确定删除该供货商吗？</p>
            <a href="#" id="yes">是</a>
            <a href="#" id="no">否</a>
        </div>
    </div>
</div>



<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/supplier/list.js"></script>

