<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/toMain"/>
    <!-- </section> -->
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">

    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>入库记录管理页面</span>
        </div>
        <div class="search">
            <form method="get" action="/store724_war/sys/storageRecord/list">
                <span>商品名称：</span>
                <input name="queryGoodsName" type="text" value="">
                <span>供货商：</span>
                <select name="querySupplierId">

                    <option value="">--全部--</option>

                    <option
                            value="11">石家庄帅益食品贸易有限公司</option>

                    <option
                            value="7">中粮集团有限公司</option>

                    <option
                            value="5">福建味美美调味品厂</option>

                    <option
                            value="13">山东豪克华光联合发展有限公司</option>

                    <option
                            value="3">乐山市泰国大米有限公司</option>

                    <option
                            value="8">慈溪市广和绿色食品厂</option>

                    <option
                            value="10">南京火头军信息技术有限公司</option>

                    <option
                            value="4">香港喜乐贸有限公司</option>

                    <option
                            value="9">优百商贸有限公司</option>

                    <option
                            value="12">北京三木堂商贸有限公司</option>


                </select>

                <span>付款状态：</span>
                <select name="queryPayStatus">
                    <option value="">--全部--</option>
                    <option value="1" >未付款</option>
                    <option value="2" >已付款</option>
                </select>
                <input type="hidden" name="pageIndex" value="1"/>
                <input	value="查 询" type="submit" id="searchbutton">
                <a href="/store724_war/sys/storageRecord/toAdd">添加</a>
            </form>
        </div>
        <!--账单表格 样式和供货商公用-->
        <table class="supplierTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="15%">入库编号</th>
                <th width="10%">商品名称</th>
                <th width="10%">金额</th>
                <th width="20%">供货商</th>
                <th width="10%">付款状态</th>
                <th width="10%">创建时间</th>
                <th width="25%">操作</th>
            </tr>

            <tr>
                <td>
                    <span>121321</span>
                </td>
                <td>
                    <span>dddd</span>
                </td>
                <td>
                    <span>11111.00</span>
                </td>
                <td>
                    <span>福建味美美调味品厂</span>
                </td>
                <td>
					<span>

						已付款
					</span>
                </td>
                <td>
					<span>
					2021-10-19
					</span>
                </td>
                <td>
                    <span><a class="viewStorageRecord" href="javascript:;" storageRecordId=2 StorageRecordcc=121321><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyStorageRecord" href="javascript:;" storageRecordId=2 StorageRecordcc=121321><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteStorageRecord" href="javascript:;" storageRecordId=2 StorageRecordcc=121321><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>1</span>
                </td>
                <td>
                    <span>1</span>
                </td>
                <td>
                    <span>1111.00</span>
                </td>
                <td>
                    <span>石家庄帅益食品贸易有限公司</span>
                </td>
                <td>
					<span>

						已付款
					</span>
                </td>
                <td>
					<span>
					2021-10-19
					</span>
                </td>
                <td>
                    <span><a class="viewStorageRecord" href="javascript:;" storageRecordId=1 StorageRecordcc=1><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyStorageRecord" href="javascript:;" storageRecordId=1 StorageRecordcc=1><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteStorageRecord" href="javascript:;" storageRecordId=1 StorageRecordcc=1><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

        </table>
        <input type="hidden" id="totalPageCount" value="1"/>






        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Insert title here</title>
            <script type="text/javascript">

            </script>
        </head>
        <body>
        <div class="page-bar">
            <ul class="page-num-ul clearfix">
                <li>共2条记录&nbsp;&nbsp; 1/1页</li>


                &nbsp;&nbsp;
            </ul>
            <span class="page-go-form"><label>跳转至</label>
	     <input type="text" name="inputPage" id="inputPage" class="page-key" />页
	     <button type="button" class="page-btn" onClick='jump_to(document.forms[0],document.getElementById("inputPage").value)'>GO</button>
		</span>
        </div>
        </body>
        <script type="text/javascript" src="/store724_war/statics/js/sysRole/rollpage.js"></script>
        </html>
    </div>
</section>

<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeBi">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>确定删除该订单吗？</p>
            <a href="#" id="yes">是</a>
            <a href="#" id="no">否</a>
        </div>
    </div>
</div>



<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/storageRecord/list.js"></script>
