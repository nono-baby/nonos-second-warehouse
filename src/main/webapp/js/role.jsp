<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>





<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/user/list"/>
    <!-- </section> -->
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>角色管理页面</span>
        </div>
        <div class="search">
            <a href="/store724_war/sys/role/toAdd" >添加角色</a>
        </div>
        <table class="supplierTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">角色编码</th>
                <th width="20%">角色名称</th>
                <th width="10%">创建时间</th>
                <th width="30%">操作</th>
            </tr>

            <tr>
                <td>
                    <span>SMBMS_ADMIN</span>
                </td>
                <td>
                    <span>管理系统员</span>
                </td>
                <td>
                    <span>2019-04-13</span>
                </td>
                <td>
                    <span><a class="modifyRole" href="javascript:;" roleid=1 rolename=管理系统员><img src="/store724_war/statics/images/xiugai.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteRole" href="javascript:;" roleid=1 rolename=管理系统员><img src="/store724_war/statics/images/schu.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>SMBMS_MANAGER</span>
                </td>
                <td>
                    <span>店长</span>
                </td>
                <td>
                    <span>2019-04-13</span>
                </td>
                <td>
                    <span><a class="modifyRole" href="javascript:;" roleid=2 rolename=店长><img src="/store724_war/statics/images/xiugai.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteRole" href="javascript:;" roleid=2 rolename=店长><img src="/store724_war/statics/images/schu.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>SMBMS_EMPLOYEE</span>
                </td>
                <td>
                    <span>店员</span>
                </td>
                <td>
                    <span>2019-04-13</span>
                </td>
                <td>
                    <span><a class="modifyRole" href="javascript:;" roleid=3 rolename=店员><img src="/store724_war/statics/images/xiugai.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteRole" href="javascript:;" roleid=3 rolename=店员><img src="/store724_war/statics/images/schu.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

        </table>
    </div>
</section>

<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeRole">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该角色吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>



<div class="div_footer">
    <footer class="footer" style="height: 30px">
        版权归诺诺
    </footer>
</div>
<script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
<script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/sysRole/list.js"></script>

