<%--
  Created by IntelliJ IDEA.
  User: Think
  Date: 2021/10/19
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>






<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/store724_war/statics/css/public.css" />
</head>
<body>
<!--头部-->
<header class="publicHeader">
    <h1>724便利店管理系统</h1>
    <div class="publicHeaderR">
        <p><span style="color: #fff21b"> admin</span> , 欢迎光临！</p>
        <a href="/store724_war/logout">登出</a>
    </div>
</header>
<!--时间-->
<section class="publicTime">
    <span id="time">2019年1月1日 11:11  星期一</span>
    <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
</section>
<!--主体内容-->
<section class="publicMian">
    <div class="left">
        <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
        <nav>
            <ul class="list">
                <li ><a href="/store724_war/sys/storageRecord/list">入库记录信息</a></li>
                <li><a href="/store724_war/sys/supplier/list">供货商信息</a></li>
                <li><a href="/store724_war/sys/user/list">用户信息</a></li>
                <li><a href="/store724_war/sys/role/list">角色管理</a></li>
                <li><a href="/store724_war/sys/user/toUpdatePwd">修改密码</a></li>
                <li><a href="/store724_war/logout">退出系统</a></li>
            </ul>
        </nav>
    </div>
    <input type="hidden" id="path" name="path" value="/store724_war"/>
    <input type="hidden" id="referer" name="referer" value="http://172.25.14.240:8080/store724_war/sys/supplier/list"/>
    <!-- </section> -->
    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>用户管理页面</span>
        </div>
        <div class="search">
            <form method="get" action="/store724_war/sys/user/list">
                <span>用户名：</span>
                <input name="queryRealName" class="input-text"	type="text" value="">

                <span>角色：</span>
                <select name="queryRoleId">

                    <option value="">--全部--</option>

                    <option
                            value="1">管理系统员</option>

                    <option
                            value="2">店长</option>

                    <option
                            value="3">店员</option>


                </select>

                <input type="hidden" name="pageIndex" value="1"/>
                <input	value="查 询" type="submit" id="searchbutton">
                <a href="/store724_war/sys/user/toAdd" >添加</a>
            </form>
        </div>
        <!--用户-->
        <table class="supplierTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="15%">账号</th>
                <th width="15%">真实姓名</th>
                <th width="10%">角色</th>
                <th width="10%">性别</th>
                <th width="10%">年龄</th>
                <th width="10%">电话</th>
                <th width="30%">操作</th>
            </tr>

            <tr>
                <td>
                    <span>liuyang</span>
                </td>
                <td>
                    <span>刘阳</span>
                </td>
                <td>
                    <span>店长</span>
                </td>
                <td>
							<span>

								男
							</span>
                </td>
                <td>
                    <span>43</span>
                </td>
                <td>
                    <span>13367890900</span>
                </td>
                <td>
                    <span><a class="viewUser" href="javascript:;" userid=11 account=liuyang><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyUser" href="javascript:;" userid=11 account=liuyang><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteUser" href="javascript:;" userid=11 account=liuyang><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>lijiangtao</span>
                </td>
                <td>
                    <span>李江涛</span>
                </td>
                <td>
                    <span>店员</span>
                </td>
                <td>
							<span>
								女

							</span>
                </td>
                <td>
                    <span>38</span>
                </td>
                <td>
                    <span>18098765434</span>
                </td>
                <td>
                    <span><a class="viewUser" href="javascript:;" userid=12 account=lijiangtao><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyUser" href="javascript:;" userid=12 account=lijiangtao><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteUser" href="javascript:;" userid=12 account=lijiangtao><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>liuzhong</span>
                </td>
                <td>
                    <span>刘忠</span>
                </td>
                <td>
                    <span>店员</span>
                </td>
                <td>
							<span>

								男
							</span>
                </td>
                <td>
                    <span>40</span>
                </td>
                <td>
                    <span>13689674534</span>
                </td>
                <td>
                    <span><a class="viewUser" href="javascript:;" userid=13 account=liuzhong><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyUser" href="javascript:;" userid=13 account=liuzhong><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteUser" href="javascript:;" userid=13 account=liuzhong><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>songke</span>
                </td>
                <td>
                    <span>宋科</span>
                </td>
                <td>
                    <span>店长</span>
                </td>
                <td>
							<span>
								女

							</span>
                </td>
                <td>
                    <span>38</span>
                </td>
                <td>
                    <span>13387676762</span>
                </td>
                <td>
                    <span><a class="viewUser" href="javascript:;" userid=15 account=songke><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyUser" href="javascript:;" userid=15 account=songke><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteUser" href="javascript:;" userid=15 account=songke><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

            <tr>
                <td>
                    <span>zhaogang</span>
                </td>
                <td>
                    <span>赵刚</span>
                </td>
                <td>
                    <span>店长</span>
                </td>
                <td>
							<span>

								男
							</span>
                </td>
                <td>
                    <span>41</span>
                </td>
                <td>
                    <span>13387676762</span>
                </td>
                <td>
                    <span><a class="viewUser" href="javascript:;" userid=10 account=zhaogang><img src="/store724_war/statics/images/view.png" alt="查看" title="查看"/></a></span>
                    <span><a class="modifyUser" href="javascript:;" userid=10 account=zhaogang><img src="/store724_war/statics/images/upd.png" alt="修改" title="修改"/></a></span>
                    <span><a class="deleteUser" href="javascript:;" userid=10 account=zhaogang><img src="/store724_war/statics/images/del.png" alt="删除" title="删除"/></a></span>
                </td>
            </tr>

        </table>
        <input type="hidden" id="totalPageCount" value="3"/>






        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Insert title here</title>
            <script type="text/javascript">

            </script>
        </head>
        <body>
        <div class="page-bar">
            <ul class="page-num-ul clearfix">
                <li>共11条记录&nbsp;&nbsp; 1/3页</li>


                <a href="javascript:page_nav(document.forms[0],2);">下一页</a>
                <a href="javascript:page_nav(document.forms[0],3);">尾页</a>

                &nbsp;&nbsp;
            </ul>
            <span class="page-go-form"><label>跳转至</label>
	     <input type="text" name="inputPage" id="inputPage" class="page-key" />页
	     <button type="button" class="page-btn" onClick='jump_to(document.forms[0],document.getElementById("inputPage").value)'>GO</button>
		</span>
        </div>
        </body>
        <script type="text/javascript" src="/store724_war/statics/js/sysRole/rollpage.js"></script>
        </html>


        <!--点击删除按钮后弹出的页面-->
        <div class="zhezhao"></div>
        <div class="remove" id="removeUse">
            <div class="removerChid">
                <h2>提示</h2>
                <div class="removeMain">
                    <p>确定删除该用户吗？</p>
                    <a href="#" id="yes">是</a>
                    <a href="#" id="no">否</a>
                </div>
            </div>
        </div>



        <div class="div_footer">
            <footer class="footer" style="height: 30px">
                版权归诺诺
            </footer>
        </div>
        <script type="text/javascript" src="/store724_war/statics/js/time.js"></script>
        <script type="text/javascript" src="/store724_war/statics/js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="/store724_war/statics/js/common.js"></script>
        <script type="text/javascript" src="/store724_war/statics/calendar/WdatePicker.js"></script>
</body>
</html>
<script type="text/javascript" src="/store724_war/statics/js/sysUser/list.js"></script>

